from flask import Flask
import calc




app = Flask(__name__)

@app.route("/")
def home():
    return "ok"

@app.route("/student/<number>")
def square(number):
    num = int(number)
    n = calc.calculCarre(num)
    return f"Le carré de {num} est {n}"


app.run(host="0.0.0.0", port=8081)