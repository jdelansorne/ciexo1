import unittest
from calc import calculCarre

class TestCalcModule(unittest.TestCase):
  def test_calculCarre(self):
    self.assertEqual(calculCarre(5), 25)